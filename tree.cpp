#include "tree.h"
using namespace std;

/*
   This tree.cpp file is to implement the functions from the tree.h file
   the each wrapper function will have a recursive funtion. Functions 
   include insertion, deletion, retrieve, and display. 
 */


// constructor
tree::tree()
{
	/*root = new node;
	  root->data.name = nullptr;
	  root->data.state = nullptr;	
	  root->data.rec = nullptr;
	  root->data.time = nullptr;
	  root->data.travel = nullptr;
	  root->data.notes = nullptr;
	  root->left = nullptr;
	  root->right = nullptr;*/
	root = nullptr;
}


// destructor
tree::~tree()
{
	remove_all(root);
}


// the recusive function for my destructor to remove all from the BST
int tree::remove_all(node *& root)
{
	if(!root)
		return 0;
	int rmL = remove_all(root->left);
	int rmR = remove_all(root->right);
	delete [] root->data.name;
	delete [] root->data.state;
	delete [] root->data.rec;
	delete [] root->data.time;
	delete [] root->data.travel;
	delete [] root->data.notes;
	delete root;
	root = nullptr;
	return 1 + rmL + rmR;
}


// insert function, takes add in info from user
int tree::insert(const trip * to_add)
{
	return insertR(root, to_add);
}


// the recursive function to the insert wrapper function
int tree::insertR(node *& root, const trip * to_add)
{
	if(!root)	// if empty fill with info
	{
		root = new node;
		root->data.name = new char[strlen(to_add->name) + 1];
		strcpy(root->data.name, to_add->name);
		root->data.state = new char[strlen(to_add->state) + 1];
		strcpy(root->data.state, to_add->state);
		root->data.rec = new char[strlen(to_add->rec) + 1];
		strcpy(root->data.rec, to_add->rec);
		root->data.time = new char[strlen(to_add->time) + 1];
		strcpy(root->data.time, to_add->time);
		root->data.travel = new char[strlen(to_add->travel) + 1];
		strcpy(root->data.travel, to_add->travel);
		root->data.notes = new char[strlen(to_add->notes) + 1];
		strcpy(root->data.notes, to_add->notes);

		root->left = nullptr;
		root->right = nullptr;	
		return 1;
	}
	int sort = strcasecmp(to_add->name, root->data.name);
	if(sort < 0)	
		return 1 + insertR(root->left, to_add);
	return 1 + insertR(root->right, to_add);

}


// function displays all the contents of the BST
int tree::display_all()
{
	if(!root)	// if tree is empty
		return 0;
	display_allR(root);
	return 1;
}


// The recursive function for display all rapper function
int tree::display_allR(node * root)
{
	if(!root)	// displayed all nothing else to display
		return 0;

	display_allR(root->left);
	cout << root->data.name << '|' << root->data.state << '|'
		<< root->data.rec << '|' << root->data.time << '|'
		<< root->data.travel << '|' << root->data.notes << endl;
	display_allR(root->right);
	return 1;
}


// function wrapper to remove a node that match return 0 if tree empty
int tree::remove_match(char * to_remove)
{	
	if(!root)	// empty tree
		return 0;
	return remove_matchR(root, to_remove);
}


// recursive of the remove wrapper that returns 0 if no match
int tree::remove_matchR(node *& root, char * to_remove)
{
	if(!root)	// there was no matches
		return 0;
	int sort = strcasecmp(to_remove, root->data.name);
	if(sort < 0)
		return remove_matchR(root->left, to_remove);
	else if(sort > 0)
		return remove_matchR(root->right, to_remove);
	else
	{
		if(!root->left && !root->right)	
		{
			delete [] root->data.name;
			delete [] root->data.state;
			delete [] root->data.rec;
			delete [] root->data.time;
			delete [] root->data.travel;
			delete [] root->data.notes;
			delete [] root;
			root = nullptr;
			return 1;
		}
		else if(!root->left && root->right)	
		{
			node * temp = root;
			root = root->right;
			delete [] temp->data.name;
			delete [] temp->data.state;
			delete [] temp->data.rec;
			delete [] temp->data.time;
			delete [] temp->data.travel;
			delete [] temp->data.notes;
			delete temp;
			return 1;
		}

		else if(!root->right && root->left)
		{
			node * temp = root;
			root = root->left;
			delete [] temp->data.name;
			delete [] temp->data.state;
			delete [] temp->data.rec;
			delete [] temp->data.time;
			delete [] temp->data.travel;
			delete [] temp->data.notes;
			delete temp;
			return 1;
		}
		else
		{
			node * current = root->right;
			while(current->left)
				current = current->left;

			delete [] root->data.name;
			delete [] root->data.state;
			delete [] root->data.rec;
			delete [] root->data.time;
			delete [] root->data.travel;	
			delete [] root->data.notes;

			root->data.name = new char[strlen(current->data.name) + 1];
			strcpy(root->data.name, current->data.name);
			root->data.state = new char[strlen(current->data.state) + 1];
			strcpy(root->data.state, current->data.state);
			root->data.rec = new char[strlen(current->data.rec) + 1];
			strcpy(root->data.rec, current->data.rec);
			root->data.time = new char[strlen(current->data.time) + 1];
			strcpy(root->data.time, current->data.time);
			root->data.travel = new char[strlen(current->data.travel) + 1];
			strcpy(root->data.travel, current->data.travel);
			root->data.notes = new char[strlen(current->data.notes) + 1];
			strcpy(root->data.notes, current->data.notes);
		}
	}
	return 1;
}


// this wrapper function will retrieve an item returning 0 if tree empty
int tree::retrieve(char * to_find, trip & found)
{
	if(!root)	// empty tree
		return 0;
	return retrieveR(root, to_find, found);	
}


// recursive funtion for wrapper finding the matching item and copy into found object
int tree::retrieveR(node * root, char * to_find, trip & found)
{
	if(!root)	// there was no match
		return 0;
	int sort = strcasecmp(to_find, root->data.name);
	if(sort < 0)
		return retrieveR(root->left, to_find, found);
	else if (sort > 0)	
		return retrieveR(root->right, to_find, found);
	else
	{
		strcpy(found.name, root->data.name);
		strcpy(found.state, root->data.state);
		strcpy(found.rec, root->data.rec);
		strcpy(found.time, root->data.time);
		strcpy(found.travel, root->data.travel);
		strcpy(found.notes, root->data.notes);
	}
	return 1;
}


// this wapper function will display matches of the time
int tree::display_match(const char * to_find)
{
	return display_matchR(root, to_find);
}


// recursive for wapper to find match and displays that match 
// continues transversal until no more matches
int tree::display_matchR(node * root, const char * to_find)
{
	if(!root)
		return 0;
	int sort = strcasecmp(to_find, root->data.time);
	int count = 0;
	if( sort == 0)
	{
		cout << root->data.name << '|' << root->data.state << '|'
			<< root->data.rec << '|' << root->data.time << '|'
			<< root->data.travel << '|' << root->data.notes << endl;
		++count;	
	}
	count += display_matchR(root->left, to_find);
	count += display_matchR(root->right, to_find);
	return count;
}


// wrapper function to determine tree height
int tree::height()
{
	return heightR(root);
}


// recursive for height, returning max hight and 0 if empty
int tree::heightR(node * root)
{
	if(!root)
		return 0;
	int heightLeft = heightR(root->left);
	int heightRight = heightR(root->right);
	if(heightLeft > heightRight)
		return 1 + heightLeft;
	else
		return 1 + heightRight;
}


// This function loads in the information from an external file
// and returns the number of entries loaded in
int tree::load(char * file)
{
	ifstream file_in(file);
	if (!file_in)
		return 0;
	int count = 0;	//counts the number of entries loaded in
	char name[SIZE];
	char state[SIZE];
	char rec[SIZE];
	char time[SIZE];
	char travel [SIZE];
	char notes[SIZE];
	trip Info;

	file_in.get(name, SIZE, '|');
	file_in.ignore(100, '|');
	Info.name = new char[strlen(name) + 1];
	strcpy(Info.name, name);

	while (file_in && !file_in.eof())
	{
		/*	file_in.get(name, SIZE, '|');		// we dont do this because we have to prime the pump
			file_in.ignore(100, '|');
			Info.name = new char[strlen(name) + 1];
			strcpy(Info.name, name);*/

		file_in.get(state, SIZE, '|');
		file_in.ignore(100, '|');
		Info.state = new char[strlen(state) + 1];
		strcpy(Info.state, state);

		file_in.get(rec, SIZE, '|');
		file_in.ignore(100, '|');
		Info.rec = new char[strlen(rec) + 1];
		strcpy(Info.rec, rec);

		file_in.get(time, SIZE, '|');
		file_in.ignore(100, '|');
		Info.time = new char[strlen(time) + 1];
		strcpy(Info.time, time);

		file_in.get(travel, SIZE, '|');
		file_in.ignore(100, '|');
		Info.travel = new char[strlen(travel) + 1];
		strcpy(Info.travel, travel);

		file_in.get(notes, SIZE, '\n');
		file_in.ignore(100, '\n');
		Info.notes = new char[strlen(notes) + 1];
		strcpy(Info.notes, notes);

		insertR(root, &Info);
		if(Info.name)		//check if filled to delete
			delete[] Info.name;
		if(Info.state)
			delete[] Info.state;
		if(Info.rec)
			delete[] Info.rec;
		if(Info.time)
			delete[] Info.time;
		if(Info.travel)
			delete[] Info.travel;
		if(Info.notes)
			delete[] Info.notes;
		file_in.get(name, SIZE, '|');
		file_in.ignore(100, '|');
		Info.name = new char[strlen(name) + 1];
		strcpy(Info.name, name);
		++count;
		/*delete[] Info.name;		// we dont want to do this bc we are deleting before it gets inserted
		  delete[] Info.state;
		  delete[] Info.rec;
		  delete[] Info.time;
		  delete[] Info.travel;
		  delete[] Info.notes;*/
	}
	file_in.close();
	return count;
}

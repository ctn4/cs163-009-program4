#include "tree.h"
using namespace std;

/*
	This main.cpp file call the functions implemented in tree.cpp
	Using a menu to test different combinations of the functions
	as well as using the result to test if function was successful
	or not. In main you are able to call functions that could insert,
	remove, retrieve, display, and height.
*/

int main()
{
	tree BST;	// tree object
	trip myTrip;	// trip object to fill and pass into argument for insert
	int result {0};	// error check if 0 there is error
	int choice {0};	// menu choice
	char name[SIZE];	// name passed into argument to find match
	trip found;	// trip object filled with info retrieved
	char yes_no = 0;	// yes or no choice for external file

	cout << "\nWelcome to the trip planner!" << endl
		<< "\nThis program will allow you to add, remove, retrieve" << endl
		<< "and display/display matches of your trip." << endl << endl;
	cout << "Here is an external file, program4.txt, with 20 set items." << endl
		<< "Would you like to load in this file into the program? [Y/N] : ";
	cin >> yes_no;

	if(toupper(yes_no) == 'Y')
	{
		char file[] = "program4.txt";
		result = BST.load(file);
		if(result != 0)
			cout << "\nFile was Loaded in" << endl;
		else if (result == 0)
			cout << "\nFile was not Loaded in" << endl;
	}	
	else 
		cout << "File was not Loaded in" << endl;
	do
	{
		cout << "\n--------MENU---------" << endl
			<< "[1] Add a trip" << endl
			<< "[2] Display all" << endl
			<< "[3] Remove a trip" << endl
			<< "[4] Retrieve a trip" << endl
			<< "[5] Display matches" << endl
			<< "[6] Height" << endl
			<< "[7] Quit" << endl;
		cout << "Enter Choice: ";
		cin >> choice;
		//cin.ignore(100, '\n');	
		while(!cin || cin.peek() != '\n')       // validate the user enters number not a char
		{
			cin.clear();
			cin.ignore(100, '\n');
			cout << "Invalid Number. Try again." << endl
		 	     << "Enter your choice: ";
			cin >> choice;
		}
		cin.get();	
		if(choice == 1)
		{
			cout << "\nEnter the name of the location: ";
			myTrip.name = new char[SIZE];
			cin.get(myTrip.name, SIZE, '\n');
			cin.ignore(100,'\n');
			cout << "Enter the state and/or country: ";
			myTrip.state = new char[SIZE];
			cin.get(myTrip.state, SIZE, '\n');
			cin.ignore(100,'\n');
			cout << "Enter the best things to do there: ";
			myTrip.rec = new char[SIZE];
			cin.get(myTrip.rec, SIZE, '\n');
			cin.ignore(100,'\n');
			cout << "Enter the best time of the year to go: ";
			myTrip.time = new char[SIZE];
			cin.get(myTrip.time, SIZE, '\n');
			cin.ignore(100,'\n');
			cout << "Enter how to travel there: ";
			myTrip.travel = new char[SIZE];
			cin.get(myTrip.travel, SIZE, '\n');
			cin.ignore(100,'\n');
			cout << "Enter any notes about the trip: ";
			myTrip.notes = new char[SIZE];
			cin.get(myTrip.notes, SIZE, '\n');
			cin.ignore(100,'\n');

			result = BST.insert(&myTrip);
			if (result == 0)
				cout << "Insert Failed" << endl;
			else
				cout << "Insert Successful" << endl;

		}
		if(choice == 2)	
		{
			result = BST.display_all();
			if(result == 0)		// empty
				cout << "\nNothing to display" << endl;
		}
		if(choice == 3)
		{
			cout << "Enter the trip you would like to remove: ";
			cin.get(name, SIZE, '\n');
			cin.ignore(100, '\n');
			result = BST.remove_match(name);
			if(result)
				cout << "Removed successfully" << endl;
			else
				cout << "Failed to remove" << endl;
		}
		if(choice == 4)
		{
			cout << "Enter the trip you want to retrieve: ";
			cin.get(name, SIZE, '\n');
			cin.ignore(100, '\n');
			found.name = new char [SIZE];
			found.state = new char [SIZE];
			found.rec = new char [SIZE];
			found.time = new char [SIZE];
			found.travel = new char [SIZE];
			found.notes = new char [SIZE];
			result = BST.retrieve(name, found);
			if(result != 0)
			{
				cout << found.name << " | " << found.state << " | "
					<< found.rec << " | " << found.time << " | "
					<< found.travel << " | " << found.notes << endl;
			}
			else if(result == 0)
				cout << "Location not found." << endl;
			delete [] found.name;
			delete [] found.state;
			delete [] found.rec;
			delete [] found.time;
			delete [] found.travel;
			delete [] found.notes;
		}
		if(choice == 5)
		{
			cout << "\nNote: If you loaded in program4.txt the time should be (fall, spring, winter, summer)"
				<< "\n\nEnter the TIME you watch to dipslay matches for: ";
			cin.get(name, SIZE, '\n');
			cin.ignore(100, '\n');
			result = BST.display_match(name);
			cout << "Total matches found: " << result << endl;
		}
		if(choice == 6)
		{
			result = BST.height();
			cout << "Height: " << result << endl;
		}

	}while(choice !=7);
	cout << "\nThank you. Have a good day!" << endl;

	return 0;
}

San Diego|California|LegoLand, Sea World|Fall|Car|Beautiful city
Los Angeles|California|Universal Studios, Hollywood Walk of Fame|Summer|Car|Busy and lively
San Francisco|California|Golden Gate Bridge, Alcatraz Island|Spring|Flight|Gorgeous views
New York City|New York|Statue of Liberty, Times Square|Fall|Flight|City that never sleeps
Chicago|Illinois|Millennium Park, Navy Pier|Summer|Train|Architectural gem
Miami|Florida|South Beach, Art Deco District|Winter|Flight|Tropical paradise
Las Vegas|Nevada|The Strip, Bellagio Fountains|Summer|Flight|Entertainment capital
Seattle|Washington|Space Needle, Pike Place Market|Summer|Flight|Coffee
Denver|Colorado|Rocky Mountain National Park, Red Rocks Park|Summer|Car|Outdoor adventure
Boston|Massachusetts|Freedom Trail, Fenway Park|Spring|Train|Rich history
New Orleans|Louisiana|French Quarter, Mardi Gras|Spring|Flight|Vibrant and lively
Nashville|Tennessee|Country Music Hall of Fame, Broadway|Fall|Car|Music
Austin|Texas|State Capitol, Lady Bird Lake|Spring|Car|Live music capital
San Antonio|Texas|The Alamo, River Walk|Spring|Car|Historical charm
Orlando|Florida|Walt Disney World, Universal Orlando Resort|Winter|Flight|Magical experience
Washington, D.C.|District of Columbia|White House, National Mall|Spring|Flight|Monuments and museums
Philadelphia|Pennsylvania|Liberty Bell, Independence Hall|Fall|Train|Birthplace of America
Honolulu|Hawaii|Waikiki Beach, Pearl Harbor|Winter|Flight|Tropical paradise
Portland|Oregon|Powell's City of Books, International Rose Test Garden|Summer|Flight| Use Trimet
Newport|Rhode Island|The Breakers, Cliff Walk|Summer|Car|Elegant coastal town

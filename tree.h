#include <iostream>
#include <cstring>
#include <cctype>
#include <fstream>
/*
   Christine Nguyen, CS163, Program 4, 06/02/2023
   This program will be using a Binary Search Tree as its data structure.
   The binary search tree will sort the order of our travel destinations.
   Each travel destination will inculde a name of the location, state/country,
   besting thing to do, best time of year to go, how to travel, and notes of
   the trip.
 */

const int SIZE {100};	// constant size of infomation of trip entry 


// struct of data infomation for trip
struct trip
{
	char * name;	// name of the trip location
	char * state;	// state/country of the location
	char * rec;	// name of the attraction
	char * time;	// best time of the year to go
	char * travel;	// how to travel to there
	char * notes;	// notes of the trip 
};


// struct for node holds trip data and a left/right pointer for BST
struct node
{
	trip data;	// object with trip struct trip info
	node * left;	// left pointer
	node * right;	// right pointer
};


// class for BST that holds the functions to manipluate the BST
class tree
{
	public:
		tree();		// constructor
		~tree();	// destructor
		int insert(const trip * to_add);	// insert a trip item
		int display_all();	// display all entries 
		int remove_match(char * to_remove);	// remove matching trip name
		int retrieve(char * to_find, trip & found);	// retrieves  a matchin item by name
		int display_match(const char * to_find);	// displays all matching items
		int height();		// finds the height of the tree
		int load(char * file);		// loads in the external file 
	private:
		node * root;	// root of tree
		int remove_all(node *& root);	// for the destructor deleting all the nodes
		int insertR(node *& root, const trip * to_add);		// recursive function for insert function 
		int display_allR(node * root);		// recursive funtion for display all entries function
		int remove_matchR(node *& root, char * to_remove);	// recursive function for removing a trip based on name
		int retrieveR(node * root, char * to_find, trip & found);	// recursive function for retrieving and filling item
		int display_matchR(node * root, const char * to_find);		// recurisve fiontion for displaying all the matches
		int heightR(node * root);	// recursive function for finding height
};
